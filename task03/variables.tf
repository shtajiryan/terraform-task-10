variable "location" {
  type        = string
  default     = "eastus"
  description = "Location for all resources."
}

variable "rg-name" {
  type    = string
  default = "cmtr6ab30b9f-rg"
}

variable "sa-name" {
  type    = string
  default = "cmtr6ab30b9fsa"
}

variable "vnet-name" {
  type    = string
  default = "cmtr6ab30b9f-vnet"
}

variable "public-subnet-name" {
  type = string
  default = "public"
}

variable "private-subnet-name" {
  type = string
  default = "private"
}